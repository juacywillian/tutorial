#!/urs/bin/env python
#-*- encoding: utf-8 -*-
# import os
import wx
from config import *


class MyApp(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(MyApp, self).__init__(*args, **kwargs)
        self.init_gui()

    def init_gui(self):
        self.SetTitle("HelloWorld")
        self.SetSize(wx.Size(800, 500))

        self.menubar = self.create_menubar()
        self.SetMenuBar(self.menubar)

        self.toolbar = self.create_toolbar()
        self.toolbar.Realize()
        self.Show()

    def create_menubar(self, ):
        menubar = wx.MenuBar()
        file = wx.Menu()

        openFile = file.Append(wx.ID_OPEN, '&Open\tCtrl+O', 'Open a new file')
        newFile = file.Append(wx.ID_OPEN, '&Open\tCtrl+O', 'Open a new file')
        saveFile = file.Append(wx.ID_OPEN, '&Open\tCtrl+O', 'Open a new file')
        saveAsFile = file.Append(wx.ID_OPEN, '&Open\tCtrl+O', 'Open a new file')
        file.AppendSeparator()
        importFile = file.Append(, '', '')
        exportFile = file.Append(, '', '')
        file.AppendSeparator()
        quitFile = file.Append(wx.ID_EXIT, '&Quit\tCtrl+Q', 'Close Application')

        self.Bind(wx.EVT_MENU, self.open_file, openFile)
        
        menubar.Append(file, '&File')

        return menubar

    def create_toolbar(self, ):
        toolbar = self.CreateToolBar()
        tool_item = toolbar.AddLabelTool(wx.ID_EXIT, 'Quit\tCtrl+Q',
                                         wx.Bitmap(IMG+'exit.png'))
        self.Bind(wx.EVT_TOOL, self.quit, id=wx.ID_EXIT)

        return toolbar

    def open_file(self, e):
        pass

    def quit(self, e):
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    MyApp(None)
    app.MainLoop()
